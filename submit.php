<!DOCTYPE html> 
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Result Form</title>
<body>
    
    <?php
        session_start();
        $Date = $_SESSION["birthOfDate"];
    ?>
    <fieldset style='max-width: 50%; max-height: 100%; border:#ADD8E6 solid'>
    <form style='margin: 20px 50px 0 35px' method="post">
        <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
            <tr height = '40px'>
                <td style = 'background-color: #1e8dcc; vertical-align: top; text-align: center; padding: 3px 15px 5px 15px;'>
                    <label style='color: white;'>Họ và tên</label>
                </td>
                <td >
                    <?php
                        echo "<div style='color: black;'>".$_SESSION["name"]."</div>"
                    ?>
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #1e8dcc; vertical-align: top; text-align: center; padding: 3px 15px 5px 15px;'>
                    <label style='color: white;'>Giới tính</label>
                </td>
                <td >
                    <?php
                        echo "<div style='color: black;'>".$_SESSION["gender"]."</div>"
                    ?>  
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 3px 15px 5px 15px;'>
                    <label style='color: white;'>Phân Khoa</label>
                </td>
                <td height = '40px'>
                    <?php
                        echo "<div style='color: black;'>".$_SESSION["industryCode"]."</div>"
                    ?>  
                </td>
            </tr>
            
            <tr height = '40px'>
                <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 3px 15px 5px 15px;'>
                    <label style='color: white;'>Ngày sinh</label>
                </td>
                <td height = '40px'>
                    <?php
        
                        echo "<div style='color: black;'>".date("d-m-Y", strtotime($Date))."</div>"
                    ?>  
                </td>
            </tr>

            <tr height = '40px'>
                <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 3px 15px 5px 15px;'>
                    <label style='color: white;'>Địa chỉ</label>
                </td>
                <td height = '40px'>
                    <?php
                        echo "<div style='color: black;'>".$_SESSION["direction"]."</div>"
                    ?>  
                </td>
            </tr>

            <tr height = '40px' >
                <td style = ' width: 60px; background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 3px 15px 5px 15px;'>
                    <label style='color: white;'>Hình ảnh </label>
                </td>
                <td height = '40px'>
                    <?php
                        echo "<img src='".$_SESSION["file"]."' width='100' height='50'>";
                    ?>  
                </td>
            </tr>

        </table>
        <button style='background-color: #49be25; border-radius: 10px; width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Đăng Kí</button>
    </form>

</fieldset>
</body>
</html>

